<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 3:26 AM
 */
namespace  app\modules\v1\resources;

use app\models\User;

class UserResources extends User
{
    public function fields()
    {
        return [
            'id', 'fullname','email', 'type','auth_key'
        ];
    }

}
