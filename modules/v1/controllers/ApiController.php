<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 2:15 AM
 */

namespace app\modules\v1\controllers;

use app\modules\v1\models\ApiLogin;
use app\modules\v1\models\ApiSignup;
use Yii;
use yii\filters\Cors;
use yii\rest\ActiveController;

class ApiController extends ActiveController
{
    public $modelClass = 'app\models\User';

     public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'cors' => Cors::class
        ]);
    }

    //Admin Log in
    public function actionLogin()
    {
        $model = new ApiLogin();
        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {
            return $model->getUser();
        }

        Yii::$app->response->statusCode = 422;
        return [
            'errors' => $model->errors
        ];
    }


    //Create a new admin
    public function actionSignup()
    {

        $model = new ApiSignup();

        if ($model->load(Yii::$app->request->post(), '') && $model->signup()) {
            return $model;
        }

        Yii::$app->response->statusCode = 422;
        return [
            'errors' => $model->errors
        ];
    }


}
