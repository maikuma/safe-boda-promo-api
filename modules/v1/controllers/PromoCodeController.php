<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 2:16 AM
 */

namespace app\modules\v1\controllers;


use yii\filters\AccessControl;
use yii\filters\Cors;
use yii\filters\VerbFilter;

class PromoCodeController extends \yii\rest\ActiveController
{

    public $modelClass = 'app\modules\admin\models\PromoCode';

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'cors' => Cors::class
        ]);
    }


    public function actionIndex()
    {
        return $this->render('index');
    }



}
