<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 2:48 AM
 */

namespace app\modules\v1\controllers;


use yii\filters\Cors;

class RideController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\modules\rider\models\RiderPromo';

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'cors' => Cors::class
        ]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
