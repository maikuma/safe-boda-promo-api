<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 3:59 AM
 */

namespace app\modules\v1\models;


use app\modules\v1\resources\UserResources;
use Yii;
use yii\base\Model;
use yii\db\Expression;

class ApiSignup extends Model
{
    public $fullname;
    public $email;
    public $password;
    public $type;

    public $user = null;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['fullname','type'], 'required'],
            [['fullname'], 'string', 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'app\models\user', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],

//            [['username', 'password', 'password_repeat'], 'required'],
//            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
//            ['username', 'unique',
//                'targetClass' => '\app\modules\api\resources\UserResource',
//                'message' => 'This username has already been taken.'
//            ],
        ];
    }

    public function signup()
    {
        if ($this->validate()) {
            $user = new UserResources();

            $user->fullname = $this->fullname;
            $user->email = $this->email;
            $user->type =$this->type;
            $user->created_at = new Expression('NOW()');
            $user->updated_at = new Expression('NOW()');
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();

            $this->user = $user;

            if ($user->save()) {
                return Yii::$app->user->login($user, 0);
            }
            return false;
        }
        return false;
    }

}
