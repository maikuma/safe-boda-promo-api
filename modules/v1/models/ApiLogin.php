<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 3:35 AM
 */

namespace app\modules\v1\models;


use app\models\LoginForm;
use app\modules\v1\resources\UserResources;

class ApiLogin extends LoginForm
{
/**
     * @return UserResources|bool|null
     * @author Kennedy Maikuma <mukhwanak@gmail.com>
     */

    public function getUser()
    {

        if ($this->_user === false) {
            $this->_user = UserResources::findByUsername($this->username);
        }

        return $this->_user;
    }
}
