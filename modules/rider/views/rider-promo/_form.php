<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\rider\models\RiderPromo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rider-promo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'promo_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pickup_latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pickup_longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'drop_latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'drop_longitude')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
