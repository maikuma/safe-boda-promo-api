<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\rider\models\RiderPromo */

$this->title = 'Update Rider Promo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rider Promos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2><h1><?= Html::encode($this->title) ?></h1></h2>
            </div>
            <div class="body">

                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>

</div>
