<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\rider\models\RiderPromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rider Used Promos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                <h2><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <p>
                        <?= Html::a('Check Promo Code', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>

                    <?php Pjax::begin(); ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
//                        'filterModel' => $searchModel,
                        'rowOptions' => function ($model, $index, $widget, $grid) {

                            if ($model->status == 1) {
                                return ['style' => 'background-color: #d2f8d2'];

                            } else {

                                return ['style' => 'background-color: #FFC1C1'];

                            }

                        },
                        'tableOptions' => [
                            'class' => 'table table-hover js-basic-example dataTable table-custom',
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

//                            'id',
//                            'rider_id',
                            'promo_code',
                            'trip_time',
                            'pickup_latitude',
                            'pickup_longitude',
                            'drop_latitude',
                            'drop_longitude',
                            'discount',
                            [
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    // Nested ternary expressions (deprecated since PHP 7.4)
                                    return $model->status == 1 ? 'Active' : 'Suspended';
                                }
                            ],
                            //'status',
                            //'created_at',
                            //'updated_at',
                            //'deleted_at',
                            //'created_by',
                            //'updated_by',
                            //'deleted_by',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>


                </div>
            </div>
        </div>
    </div>

</div>
