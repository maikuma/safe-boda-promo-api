<?php

namespace app\modules\rider\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\rider\models\RiderPromo;

/**
 * RiderPromoSearch represents the model behind the search form of `app\modules\rider\models\RiderPromo`.
 */
class RiderPromoSearch extends RiderPromo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'rider_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['promo_code', 'trip_time', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['pickup_latitude', 'pickup_longitude', 'drop_latitude', 'drop_longitude', 'discount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RiderPromo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'rider_id' => $this->rider_id,
            'trip_time' => $this->trip_time,
            'pickup_latitude' => $this->pickup_latitude,
            'pickup_longitude' => $this->pickup_longitude,
            'drop_latitude' => $this->drop_latitude,
            'drop_longitude' => $this->drop_longitude,
            'discount' => $this->discount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'promo_code', $this->promo_code]);

        return $dataProvider;
    }
}
