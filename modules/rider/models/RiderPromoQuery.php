<?php

namespace app\modules\rider\models;

/**
 * This is the ActiveQuery class for [[RiderPromo]].
 *
 * @see RiderPromo
 */
class RiderPromoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return RiderPromo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return RiderPromo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
