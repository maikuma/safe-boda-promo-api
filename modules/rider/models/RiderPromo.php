<?php

namespace app\modules\rider\models;

use app\models\User;
use app\modules\admin\models\PromoCode;
use app\Traits\CreatorTrait;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "rider_promo".
 *
 * @property int $id
 * @property int $rider_id Rider using the promo code
 * @property string $promo_code This Promotion Code
 * @property string $trip_time The Exact time the code was used
 * @property float $pickup_latitude This is the latitude  of the Pick location that that the Promo will be used
 * @property float $pickup_longitude This is the longitude  of the Pick location that that the Promo will be used
 * @property float $drop_latitude This is the latitude  of the Drop location that that the Promo will be used
 * @property float $drop_longitude This is the longitude  of the Drop location that that the Promo will be used
 * @property float $discount The distance in radius that the promo is valid for
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int|null $deleted_by
 *
 * @property User $createdBy
 * @property PromoCode $promoCode
 * @property User $rider
 * @property User $updatedBy
 */
class RiderPromo extends \yii\db\ActiveRecord
{
    use CreatorTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    public $offer = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rider_promo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_code', 'trip_time', 'pickup_latitude', 'pickup_longitude', 'drop_latitude', 'drop_longitude'], 'required'],
            [['rider_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['rider_id', 'trip_time', 'created_at', 'updated_at', 'deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by', 'discount'], 'safe'],
            [['pickup_latitude', 'pickup_longitude', 'drop_latitude', 'drop_longitude', 'discount'], 'number'],
            [['promo_code'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['promo_code'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCode::className(), 'targetAttribute' => ['promo_code' => 'promo_code']],
            [['rider_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rider_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rider_id' => 'Rider',
            'promo_code' => 'Promo Code',
            'trip_time' => 'Trip Time',
            'pickup_latitude' => 'Pickup Latitude',
            'pickup_longitude' => 'Pickup Longitude',
            'drop_latitude' => 'Drop Latitude',
            'drop_longitude' => 'Drop Longitude',
            'discount' => 'Discount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }

    public function getdiscount()
    {
        if ($this->validate()) {

            $rider = new RiderPromo();
            $pick_up = $rider->applypromo($rider->promo_code, $rider->pickup_latitude, $rider->pickup_longitude);
            $drop = $rider->applypromo($rider->promo_code, $rider->drop_latitude, $rider->drop_longitude);

            if ($pick_up >= 0) {

                $discount = $pick_up;

            } elseif ($drop >= 0) {

                $discount = $drop;

            }

            if ($discount >= 0) {

                $rider->discount = $discount;
                $rider->rider_id = Yii::$app->user->identity->id;
                $rider->trip_time =  new Expression('NOW()');


                if ($offer = $rider->save()) {

                    return $offer;
                }
                return false;

            }

            return false;
        }

        return false;

    }

    public function applypromo($promo, $lat1, $lon1)
    {
        $coordinates = PromoCode::findOne(['promo_code' => $promo, 'status' => self::STATUS_ACTIVE]);

        if (!empty($coordinates)) {

            $distances = $this->distance($lat1, $lon1, $coordinates["event_latitude"], $coordinates["event_longitude"]);

            if ($distances <= $coordinates["distance"]) {

                $discount = $coordinates["amount"];

            } else {

                $discount = 0;
            }


        } else {
            $discount = 0;

        }

        return $discount;


    }

    public function distance($lat1, $lon1, $lat2, $lon2)
    {
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lon1 *= $pi80;
        $lat2 *= $pi80;
        $lon2 *= $pi80;
        $r = 6372.797; // mean radius of Earth in km
        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km = $r * $c;


        //echo ' '.$km;
        return $km;
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[PromoCode]].
     *
     * @return \yii\db\ActiveQuery|PromoCodeQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::className(), ['promo_code' => 'promo_code']);
    }

    /**
     * Gets query for [[Rider]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getRider()
    {
        return $this->hasOne(User::className(), ['id' => 'rider_id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return RiderPromoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RiderPromoQuery(get_called_class());
    }
}
