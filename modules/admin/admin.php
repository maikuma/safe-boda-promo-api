<?php

namespace app\modules\admin;

/**
 * admin module definition class
 */
class admin extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here

//        $this->setComponents([
//            // array of components
//            'admin' => [
//                'identityClass' => 'app\modules\admin\models\Admin',
//                'enableAutoLogin' => true,
//                'identityCookie' => ['name' => '_identity-admin', 'httpOnly' => true],
//            ],
//        ]);
    }
}
