<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\PromoCode;

/**
 * PromoCodeSearch represents the model behind the search form of `app\modules\admin\models\PromoCode`.
 */
class PromoCodeSearch extends PromoCode
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['promo_name', 'promo_code', 'validity', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['event_latitude', 'event_longitude', 'distance', 'amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoCode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'validity' => $this->validity,
            'event_latitude' => $this->event_latitude,
            'event_longitude' => $this->event_longitude,
            'distance' => $this->distance,
            'amount' => $this->amount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'promo_name', $this->promo_name])
            ->andFilterWhere(['like', 'promo_code', $this->promo_code]);

        return $dataProvider;
    }
}
