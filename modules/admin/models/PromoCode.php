<?php

namespace app\modules\admin\models;

use app\models\User;
use app\modules\rider\models\RiderPromo;
use app\Traits\CreatorTrait;
use Yii;

/**
 * This is the model class for table "promo_code".
 *
 * @property int $id
 * @property string $promo_name This Promotion name
 * @property string $promo_code This is the code riders will use
 * @property string $validity The date the code will expire
 * @property float $event_latitude This is the latitude  of the location that that the Promo will be used
 * @property float $event_longitude This is the longitude  of the location that that the Promo will be used
 * @property float $distance The distance in radius that the promo is valid for
 * @property float $amount The Amount to be paid for the trip based on the code
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int|null $deleted_by
 *
 * @property User $createdBy
 * @property RiderPromo[] $riderPromos
 * @property User $updatedBy
 */
class PromoCode extends \yii\db\ActiveRecord
{
    use CreatorTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_name', 'promo_code', 'validity', 'event_latitude', 'event_longitude', 'distance', 'amount'], 'required'],
            [['validity', 'created_at', 'updated_at', 'deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['event_latitude', 'event_longitude', 'distance', 'amount'], 'number'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['promo_name', 'promo_code'], 'string', 'max' => 255],
            [['promo_name'], 'unique'],
            [['promo_code'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_name' => 'Promo Name',
            'promo_code' => 'Promo Code',
            'validity' => 'Validity',
            'event_latitude' => 'Event Latitude',
            'event_longitude' => 'Event Longitude',
            'distance' => 'Distance KM',
            'amount' => 'Amount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[RiderPromos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRiderPromos()
    {
        return $this->hasMany(RiderPromo::className(), ['promo_code' => 'promo_code']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
