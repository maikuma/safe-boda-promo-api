<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;

//use yii\widgets\ActiveForm;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\PromoCode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'promo_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'promo_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'validity')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter event time ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?= $form->field($model, 'event_latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'distance')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?php $status = array('1' => 'Active','2' => 'Suspended' ); ?>

    <?= $form->field($model, 'status')->dropDownList($status, array('prompt' => 'Select a Status:')); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


