<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%promo_code}}`.
 */
class m211012_133051_create_promo_code_table extends Migration
{
    /**
     * {@inheritdoc}
     */
        public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%promo_code}}', [
            'id' => $this->primaryKey(),
            'promo_name' => $this->string()->unique()->notNull()->comment('This Promotion name'),
            'promo_code' => $this->string()->notNull()->unique()->comment('This is the code riders will use'),
            'validity'=> $this->dateTime()->notNull()->comment('The date the code will expire'),
            'event_latitude'=>$this->float()->notNull()->comment('This is the latitude  of the location that that the Promo will be used'),
            'event_longitude'=>$this->float()->notNull()->comment('This is the longitude  of the location that that the Promo will be used'),
            'distance'=>$this->float()->notNull()->comment('The distance in radius that the promo is valid for'),
            'amount'=>$this->float()->notNull()->comment('The Amount to be paid for the trip based on the code'),
            'status' => $this->smallInteger()->notNull()->defaultValue(2),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'deleted_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `created_by` in promo_code
        $this->createIndex(
            '{{%idx-promo_code-created_by}}',
            '{{%promo_code}}',
            'created_by'
        );

        // add foreign key for table `{{%promo_code}}`
        $this->addForeignKey(
            '{{%fk-promo_code-created_by}}',
            '{{%promo_code}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by` in promo_code
        $this->createIndex(
            '{{%idx-promo_code-updated_by}}',
            '{{%promo_code}}',
            'updated_by'
        );

        // add foreign key for table `{{%promo_code}}`
        $this->addForeignKey(
            '{{%fk-promo_code-updated_by}}',
            '{{%promo_code}}',
            'updated_by',
            '{{%user}}',
            'id',
            'no action'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%promo_code}}`
        $this->dropForeignKey(
            '{{%fk-promo_code-created_by}}',
            '{{%promo_code}}'
        );

        // drops index for column `created_by` in promo_code
        $this->dropIndex(
            '{{%idx-promo_code-created_by}}',
            '{{%promo_code}}'
        );

        // drops foreign key for table `{{%promo_code}}`
        $this->dropForeignKey(
            '{{%fk-promo_code-updated_by}}',
            '{{%promo_code}}'
        );

        // drops index for column `updated_by` in promo_code
        $this->dropIndex(
            '{{%idx-promo_code-updated_by}}',
            '{{%promo_code}}'
        );

        $this->dropTable('{{%promo_code}}');
    }
}
