<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rider_promo}}`.
 */
class m211012_133130_create_rider_promo_table extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rider_promo}}', [
            'id' => $this->primaryKey(),
            'rider_id' => $this->integer()->notNull()->comment('Rider using the promo code'),
            'promo_code' => $this->string()->notNull()->comment('This Promotion Code'),
            'trip_time' => $this->timestamp()->notNull()->comment('The Exact time the code was used'),
            'pickup_latitude' => $this->float()->notNull()->comment('This is the latitude  of the Pick location that that the Promo will be used'),
            'pickup_longitude' => $this->float()->notNull()->comment('This is the longitude  of the Pick location that that the Promo will be used'),
            'drop_latitude' => $this->float()->notNull()->comment('This is the latitude  of the Drop location that that the Promo will be used'),
            'drop_longitude' => $this->float()->notNull()->comment('This is the longitude  of the Drop location that that the Promo will be used'),
            'discount' => $this->float()->notNull()->comment('The distance in radius that the promo is valid for'),
            'status' => $this->smallInteger()->notNull()->defaultValue(2),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'deleted_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `rider_id` in rider_promo
        $this->createIndex(
            '{{%idx-rider_promo-rider_id}}',
            '{{%rider_promo}}',
            'rider_id'
        );

        // add foreign key for table `{{%rider_promo}}`
        $this->addForeignKey(
            '{{%fk-rider_promo-rider_id}}',
            '{{%rider_promo}}',
            'rider_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `promo_code` in rider_promo
        $this->createIndex(
            '{{%idx-rider_promo-promo_code}}',
            '{{%rider_promo}}',
            'promo_code'
        );

        // add foreign key for table `{{%rider_promo}}`
        $this->addForeignKey(
            '{{%fk-rider_promo-promo_code}}',
            '{{%rider_promo}}',
            'promo_code',
            '{{%promo_code}}',
            'promo_code',
            'CASCADE'
        );

        // creates index for column `created_by` in rider_promo
        $this->createIndex(
            '{{%idx-rider_promo-created_by}}',
            '{{%rider_promo}}',
            'created_by'
        );

        // add foreign key for table `{{%rider_promo}}`
        $this->addForeignKey(
            '{{%fk-rider_promo-created_by}}',
            '{{%rider_promo}}',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by` in rider_promo
        $this->createIndex(
            '{{%idx-rider_promo-updated_by}}',
            '{{%rider_promo}}',
            'updated_by'
        );

        // add foreign key for table `{{%rider_promo}}`
        $this->addForeignKey(
            '{{%fk-rider_promo-updated_by}}',
            '{{%rider_promo}}',
            'updated_by',
            '{{%user}}',
            'id',
            'no action'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%rider_promo}}`
        $this->dropForeignKey(
            '{{%fk-rider_promo-rider_id}}',
            '{{%rider_promo}}'
        );

        // drops index for column `rider_id` in rider_promo
        $this->dropIndex(
            '{{%idx-rider_promo-rider_id}}',
            '{{%rider_promo}}'
        );

        // drops foreign key for table `{{%rider_promo}}`
        $this->dropForeignKey(
            '{{%fk-rider_promo-promo_code}}',
            '{{%rider_promo}}'
        );

        // drops index for column `promo_code` in rider_promo
        $this->dropIndex(
            '{{%idx-rider_promo-promo_code}}',
            '{{%rider_promo}}'
        );

        // drops foreign key for table `{{%rider_promo}}`
        $this->dropForeignKey(
            '{{%fk-rider_promo-created_by}}',
            '{{%rider_promo}}'
        );

        // drops index for column `created_by` in rider_promo
        $this->dropIndex(
            '{{%idx-rider_promo-created_by}}',
            '{{%rider_promo}}'
        );

        // drops foreign key for table `{{%rider_promo}}`
        $this->dropForeignKey(
            '{{%fk-rider_promo-updated_by}}',
            '{{%rider_promo}}'
        );

        // drops index for column `updated_by` in rider_promo
        $this->dropIndex(
            '{{%idx-rider_promo-updated_by}}',
            '{{%rider_promo}}'
        );

        $this->dropTable('{{%rider_promo}}');
    }
}
