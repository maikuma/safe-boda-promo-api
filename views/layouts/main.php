<?php
/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;

use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="theme-orange">
<?php $this->beginBody() ?>

<div id="wrapper">
    <?= $this->render('NavBar.php'); ?>
    <div id="main-content">
        <div class="container">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i
                                        class="fa fa-arrow-left"></i></a> <?= $this->title ?></h2>

                        <?= Breadcrumbs::widget([
                            'itemTemplate' => "\n\t<li class=\"breadcrumb-item\"><i>{link}</i></li>\n", // template for all links
                            'activeItemTemplate' => "\t<li class=\"breadcrumb-item active\">{link}</li>\n", // template for the active link
                            'links' => $this->params['breadcrumbs'] ?? [],
                        ]) ?>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-12 text-right">

                    </div>
                </div>
            </div>
            <!--  Main Content Page-->

            <?= $content ?>

        </div>
    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
