<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/13/2020
 * Time: 5:42 PM
 */

//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

?>
<nav class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-brand">
                <a href="<?= Yii::$app->homeUrl ?>"><img src="https://safeboda.com/ke/images/logo.png" alt="Safe Boda" class="img-responsive logo"></a>
            </div>

            <div class="navbar-right">


                <div id="navbar-menu">
                    <ul class="nav navbar-nav">

                        <li><a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>"  data-method="post" class="icon-menu d-none d-sm-none d-md-none d-lg-block"><i class="icon-login"></i></a></li>

                    </ul>
                </div>
            </div>

            <div class="navbar-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                    <i class="lnr lnr-menu fa fa-bars"></i>
                </button>
            </div>
        </div>
    </nav>

    <div class="main_menu">
        <nav class="navbar navbar-expand-lg">
            <div class="container">

                <div class="navbar-collapse align-items-center collapse" id="navbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown mega-menu">
                            <a href="<?= \yii\helpers\Url::to(['/admin/promo/']) ?>" class="nav-link" ><i class="icon-speedometer"></i> <span> Promo Code</span></a>
                        </li>
                        <li class="nav-item dropdown mega-menu">
                            <a href="<?= \yii\helpers\Url::to(['/rider/rider-promo']) ?>" class="nav-link"><i class="icon-list"></i> <span> Ride</span></a>
                        </li>

                    </ul>
                </div>

            </div>
        </nav>
    </div>
