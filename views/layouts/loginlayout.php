<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/13/2020
 * Time: 5:04 PM
 */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

// get the layout(Css and Js) for the page
use app\assets\LoginAsset;

LoginAsset::register($this);

//Page Begins
$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Lucid Bootstrap 4.1.1 Admin Template">
    <meta name="author" content="WrapTheme, design by: ThemeMakker.com">

    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="theme-orange">
<?php $this->beginBody() ?>
<!-- WRAPPER -->
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle auth-main">
            <div class="auth-box">
                <div class="top">
                    Safe Boda
                </div>

                <?= Alert::widget() ?>
                <?= $content ?>

            </div>
        </div>
    </div>
</div>
<!-- END WRAPPER -->

<!-- Javascript -->

<!-- </body> -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
