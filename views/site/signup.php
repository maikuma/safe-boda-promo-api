<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/13/2021
 * Time: 4:21 PM
 */
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \app\models\SignupForm */


use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];
?>

<div class="card">
    <div class="header">
        <p class="lead">Create an account</p>
    </div>
    <div class="body">

        <?php $form = ActiveForm::begin(['id' => 'form-signup', 'class' => 'form-auth-small', 'enableClientValidation' => false]); ?>
        <div class="form-group">
            <label for="signup-email" class="control-label sr-only">Full Name</label>
            <?= $form->field($model, 'fullname', $fieldOptions3)->textInput(['autofocus' => true, 'placeholder' => 'Full Name', 'class' => 'form-control'])->label(false) ?>
        </div>
        <div class="form-group">
            <label for="signup-password" class="control-label sr-only">Email</label>
            <?= $form->field($model, 'email', $fieldOptions1)->textInput(['autofocus' => true, 'placeholder' => 'Email', 'class' => 'form-control'])->label(false) ?>
        </div>
        <div class="form-group">
            <label for="signup-email" class="control-label sr-only">Type</label>
            <?php $type = array('rider' => 'rider','admin' => 'admin' ); ?>

            <?= $form->field($model, 'type', $fieldOptions3)->dropDownList($type, array('prompt' => 'Select a Type:'))->label(false) ?>
        </div>
        <div class="form-group">
            <label for="signup-password" class="control-label sr-only">Password</label>
            <?= $form->field($model, 'password', $fieldOptions2)->passwordInput(['hint' => 'Password', 'placeholder' => 'Password', 'class' => 'form-control'])->label(false) ?>
        </div>
        <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>


        <?php ActiveForm::end(); ?>

        <div class="bottom">
                <span class="helper-text">Already have an account? <a
                            href="<?php echo Url::toRoute(['/site/login']); ?>">Login</a></span>
        </div>

        <div class="mt-4 mt-md-5 text-center">
            <p class="mb-0">©
                <script>document.write(new Date().getFullYear())</script>
                Work With <i class="mdi mdi-heart text-danger"></i> Safe Boda
            </p>
        </div>

    </div>
</div>


