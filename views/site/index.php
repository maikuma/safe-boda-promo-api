<?php

/* @var $this yii\web\View */

use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Safe Boda Promos';
?>
<div class="promo-code-index">


</div>
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="header">
                <h2><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="body">
                <div class="table-responsive">

                    <?php Pjax::begin(); ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
//                        'filterModel' => $searchModel,
                        'rowOptions' => function ($model, $index, $widget, $grid) {

                            if ($model->status == 1) {
                                return ['style' => 'background-color: #d2f8d2'];

                            } else {

                                return ['style' => 'background-color: #FFC1C1'];

                            }

                        },
                        'tableOptions' => [
                            'class' => 'table table-hover js-basic-example dataTable table-custom',
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id',
                            'promo_name',
                            'promo_code',
                            'validity',
                            'event_latitude',
                            'event_longitude',
                            'distance',
                            'amount',
                            //'status',
                            [
                                'attribute' => 'status',
                                'value' => function ($model) {
                                    // Nested ternary expressions (deprecated since PHP 7.4)
                                    return $model->status == 1 ? 'Active' : 'Suspended';
                                }
                            ],
                            //'created_at',
                            //'updated_at',
                            //'deleted_at',
                            //'created_by',
                            //'updated_by',
                            //'deleted_by',

                            //['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>


                </div>
            </div>
        </div>
    </div>

</div>
