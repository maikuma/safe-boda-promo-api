<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 10/14/2021
 * Time: 3:16 PM
 */

namespace app\models;
use app\modules\admin\models\SetupCountry;
use app\Traits\CreatorTrait;
use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class SignupForm extends Model
{
    use CreatorTrait;

    public $fullname;
    public $email;
    public $password;
    public $type;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'type'], 'required'],
            [['fullname','type'], 'string', 'max' => 255],

//            ['username', 'trim'],
//            ['username', 'required'],
//            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
//            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->fullname = $this->fullname;
        $user->email = $this->email;
        $user->type=$this->type;
        $user->created_at = new Expression('NOW()');
        $user->updated_at = new Expression('NOW()');
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        return $user->save();
//            && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
